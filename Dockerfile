FROM php:7.1.24

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libicu-dev \
        libicu57 \
        zlib1g-dev \
        zlib1g \
        libpng-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install \
        intl \
        mbstring \
        pdo_mysql \
        zip \
        gd \
    && apt-get purge -y zlib1g-dev libicu-dev
